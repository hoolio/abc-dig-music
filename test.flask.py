#!/usr/bin/python

testfoo = "blah blah"

from flask import Flask
from flask import render_template

foo = "poo"

app = Flask(__name__)

@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name, foo=foo)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
