#!/usr/bin/python -u
#
# ABC Music HTTP server Applet
#  Copyright (C) 2013 Julius Roberts
#
# Most code borrowed from:
#  Dig Jazz Applet 
#  Copyright (C) 2007 Ian Wienand
#  http://www.wienand.org/junkcode/dig-jazz-applet/
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import urllib
import simplejson as json
import math
import time
import sys
from flask import Flask
from flask import render_template

# our own module
import abcdigapi

app = Flask(__name__)
@app.route("/test")
def test():
	return "test"
	
@app.route("/ruff")
def ruff():
	return abcdigapi._whats_on_now()
	
@app.route("/smoove")
def smoove():
	songs = abcdigapi._parse_input()
	song = songs[0]
	itunes_url = abcdigapi._craft_itunes_url(song["artistName"])
	return render_template('abcdigapi.html', REFRESH_INTERVAL=abcdigapi.REFRESH_INTERVAL, title=song["title"], artistName=song["artistName"],albumName=song["albumName"],dateCopyrighted=song["dateCopyrighted"],publisher=song["publisher"],itunes_url=itunes_url)

if __name__ == "__main__":
	app.run(debug=True, host='0.0.0.0')