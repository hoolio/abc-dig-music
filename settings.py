#!/usr/bin/python -u
#
# ABC Music Settings 
# https://github.com/hooliowobbits/abc-dig-music
#
#  Copyright (C) 2013 Julius Roberts, hooliowobbits@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. etc.

# HOW IT ALL WORKS
#	see the section by this title in README

# DIG_STATUS_XML
#	simply which of the ABC api's should we pull data from?  any will do, the API is the same.
#	most all testing was done against http://abcdigmusic.net.au/player-data.php however.

DIG_STATUS_XML = "http://abcdigmusic.net.au/player-data.php"
# DIG_STATUS_XML = "http://abcdigmusic.net.au/player-data.php"
# DIG_STATUS_XML = "http://abcjazz.net.au/player-data.php"
# DIG_STATUS_XML = "http://abccountry.net.au/player-data.php"

# Display stuff
#	simply, a toggle wether to grab data from remote sources to augment display. 
#   adds value, but also screen clutter.  most terminals support control + click to follow URL.
display_itunes_url=False
display_wikipedia_url=False

# transmission_delay 
#	we grab new song data from the ABC after every track change.  this setting controls
#   how long after that track change we should wait.  the higher the value, the longer we wait.
# 	users should alter this until track change info is synchronized with actual track change.
#   a setting of zero would result in no new track data, and the program would refresh itself
#	after the interval defined as sleep_after_no_new_track_info
# 		x) it allows time for the ABC API to refresh after a song finishes.
# 		x) also helps compensate for any transmission delay for the particular transmission medium
#			it seems that for example, we get audio "sooner" via dvb-t than via a shoutcast stream.

#transmission_delay = 20 # good for dvb-t stream
transmission_delay = 28 # good for shoutcast stream

# sleep_after_no_new_track_info
#	if we check the ABC api for new data, but don't get any; how long do we wait before trying again.
#	setting this too low will result in hammering the API occasionally, which seems to yeild
#	JSON Decode errors, see below.
sleep_after_no_new_track_info = 15

# sleep_after_json_decode_failed
#	if we start getting JSON Decode errors we know the API doesn't like us.  If we don't wait we get
#   HTTP Error 503 - Service unavailable from the ABC api.  not sure if that's a crash or a denial.
#	this is a last ditch effort to stop it all turning to poo.  a large value here is good.
sleep_after_json_decode_failed = 60

# NOTE; transmission_delay and sleep_after_no_new_track_info interact.
#	setting transmission_delaytoo low will mean we hit the API too soon, 
#	and get no new data.  if we get no new data then sleep_after_no_new_track_info
#	will determine how often we RE-HIT the api (again and again) until we DO get new data.
#	and doing that too often will yeild HTTP Error 503 - Service unavailable from the ABC api. K ?

# global_update_interval
#	How often to refresh the screen, a setting of 1 will update the info every second.  
#	There's little point in changing this at all and doing so may yeild odd results.
global_update_interval = 1
