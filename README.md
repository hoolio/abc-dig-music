abc-dig-music
=============
Copyright (C) 2013 Julius Roberts
	https://github.com/hooliowobbits/abc-dig-music
Some code from "Dig Jazz Applet" Copyright (C) 2007 Ian Wienand
	http://www.wienand.org/junkcode/dig-jazz-applet/

ABOUT:
ABC dig music plays great songs, but don't mention the track titles.  This CLI tool grabs them for you.
it actually just parses http://abcdigmusic.net.au/player-data.php into a human readable format.  
It will work with at least the following ABC now playing urls;
http://abcdigmusic.net.au/player-data.php
http://abcjazz.net.au/player-data.php
http://abccountry.net.au/player-data.php

TESTED WITH:
macOSx 10.6, ubuntu 12.04, windows 7.

DEPENDENCIES:
This software will run on many operating systems, assuming the following dependencies are met.
Exact install instructions will very between operattng systems, but have been tested OK.

x) python 2.x (should be already installed for macos, linux.  see http://www.python.org/getit/‎)
x) git (via your package manager, or download from http://git-scm.com/downloads‎/)
x) pip (a python module to help you install other python modules)

NOTE: Windows users will need to add python and git to their PATH statement.

HOW TO INSTALL:
From now on all the work happens at your command line/terminal.  
~$ pip install simplejson
~$ pip install flask
~$ git clone https://github.com/hooliowobbits/abc-dig-music

HOW IT WORKS:
./cli.py will visit the ABC API a bit after each song ends and attempt to get new song info.
Even assuming the time is correct on your computer, the time when you *hear* a song end, and 
the time when the ABC API *says* it will end will be different.  This difference
can be tuned by modifying transmission_delay_buffer_factor_in_seconds.  See settings.py

./cli.py will try and make intelligent choices as to when to talk to the ABC API.
Sometimes it will hit the ABC API and not get anything new back, so it waits a bit.
other times the ABC API says a new song will start in 10 seconds, yet the current will not finish
for another minute!  This is a bug in the ABC API, named herein "THE_EARLY_BUG".  ./cli.py will
wait the full minute in that case, and in that case start_correction would be 60 seconds.  
Worst case it will hit the API too often, the API will timeout and ./cli.py will need
to be restarted.  You can press ctrl+c to kill it.

SETTINGS:
open ~/abc-dig-music/settings.py and make sure DIG_STATUS_XML is correct for you.  The default
will be for http://abcdigmusic.net.au/player-data.php, if you like DIG then you don't
need to change anything.

HOW TO USE THE COMMAND LINE VERSION:
~$ cd abc-dig-music
abc-dig-music$ ./cli.py
Time:	13:56:47, updating from ABC in 178 seconds.
Meta:	delay buffer is 28 seconds + start correction of 0

Was:	'Detroit Swing 66' by Gomez at 13:51:42

Now:	'All I Know' by Eagle And The Worm
Album:	'Good Times' (2011) on Cotillion/Warner
Start:	13:55:44 (duration 214)

Next: 	'(Ain't Got Nobody) Just A Rambling Man' by Mountain Mocha Kilamanjaro at 13:59:18
Start:	13:59:18 adv, 13:59:18 calc, 0 diff

HOW TO USE THE WEB INTERFACE:
~$ cd abc-dig-music
abc-dig-music$ ./http.py
 * Running on http://0.0.0.0:5000/
 * Restarting with reloader
127.0.0.1 - - [24/Apr/2013 19:44:23] "GET /smoove HTTP/1.1" 500 -

..and and then visit the ip of that computer at http://x.x.x.x/smoove

KNOWN ISSUES:
Negative values for next_song_starting_in_seconds_countdown will cause API floods and timeouts.

UPSTREAM ISSUES:
occasionally the ABC dig api presents the start time of the next track
 far too soon.  that means we start grabbing data too early

POSSIBLE CHANGES:
count ABC API hit/misses and produce a ratio.
programatically adjust the buffer delay if we get too many API misses
change track: to now: in the display
