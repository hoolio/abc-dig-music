#!/usr/bin/python -u
#
# ABC Music CLI Applet
# https://github.com/hooliowobbits/abc-dig-music
#
#  Copyright (C) 2013 Julius Roberts, hooliowobbits@gmail.com
#
# Most code borrowed from:
#  Dig Jazz Applet 
#  Copyright (C) 2007 Ian Wienand
#  http://www.wienand.org/junkcode/dig-jazz-applet/
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# import some python modules.  if these don't exist on your machine they will
#  need to be installed.  first install pip, then use pip install <module>
#  installing python pip should be documented on the internet somewhere.  google it.

import urllib
import simplejson as json
import math
import time
import sys
import os

# color terminal foo
from colorama import init
init()
from colorama import Fore, Back, Style

# import our own modules
import abcdigapi
import settings

# FYI: What we get back from the ABC is something that looks like this.  Any of these attributes 
#  can be displayed, for the previous, current, and next songs.
#	{'publisher': 'Columbia', 
#	'dateCopyrighted': '1969', 
#	'links': [], 
#	'title': 'Soul Sacrifice', 
# 	'trackId': '3863-001',
#	'artistNote': None,
#	'albumName': 'Santana',
#	'artistName': 'Santana', 
#	'playedTime': '13660267',
#	'albumImage': 'http://www.abc.net.au/dig/covers/santana_1.jpg', 
#	'duration': '396',
#	'trackNote': '<a href="http://www.santana.com" target= "_blank">Santana Website</a> ',
# 	'playing': 'now'}

previous_song_track_id="0"
abc_hit_count=0
abc_miss_count=0

# just loop forever.
while True:
	songs = abcdigapi._parse_input()
	current_song = songs[0]
	next_song = songs[2]
	previous_song = songs[1]
	
	# sometimes the ABC API doesn't refresh in a timeley matter, this check makes sure we are getting new
	#  track information.  If not, wait, then try again.
	if current_song["trackId"] == previous_song_track_id:
		# then this isn't a new song.  we need to wait awhile and break the while iteration and try again
		print "   we didn't get new song info, sleeping %s seconds" % (settings.sleep_after_no_new_track_info)
		time.sleep(settings.sleep_after_no_new_track_info)
		abc_miss_count = abc_miss_count + 1
		continue
	else:
		# this is a new song, so we need to forget the old one ..
		previous_song_track_id = current_song["trackId"]
		abc_hit_count = abc_hit_count + 1

	# GRAB EXTERNAL DATA IF WE'VE BEEN CONFIGURED TO
	if settings.display_itunes_url is True: current_song_itunes_url = abcdigapi._craft_itunes_url(current_song["artistName"])
	if settings.display_itunes_url is True: current_song_wikipedia_url = abcdigapi._craft_wikipedia_url(current_song["artistName"])
		
	# WORK OUT WHEN WE SHOULD GO AHEAD AND GET MORE DATA FROM THE ABC
	#	we do that by getting the start time of the next_song, and subtracting it from the time now.
	#	if WE GET A NEGATIVE VALUE HERE it means the song should have started already but hasn't.
	#	this is an instance of THE_EARLY_BUG.  
	#	we fix that by ..?
	next_song_starting_in_seconds_countdown = (next_song["playedTime"]-time.time()) + settings.transmission_delay
	if next_song_starting_in_seconds_countdown < 0: 				
		print " ..next_song_starting_in_seconds_countdown = %s" % next_song_starting_in_seconds_countdown
			
	# SOMETIMES WE GET INCORRECT NEXT_SONG START TIME FROM ABC.  Let's call this THE_EARLY_BUG
	#	Usually far too soon. eg;
	#		the current song started at say 08:30:00, with a duration of 350 seconds.
	#		the next song will start at say 08:30:23.
	#			^^^ huh??
	#	our code waits until a bit after the next song start to go and get more data
	#		but in this instance, when we do, the data hasn't changed.  so we try again etc.
	#		once we do that a few times the api seems to timeout and then it all goes to poo.
	#	instead, we get smart.  we CALCULATE the next song time by adding the start of the current
	#		song to the duration of the current song.
	next_song_start_calculated=int(current_song["playedTime"]) + int(current_song["duration"])
	
	# THEN CALCULATE THE DIFFERENCE BETWEEN THE TWO START TIMES.  
	#	compare the calculated next song start time with the advertised next song start time to produce a diff.
	#		if the diff is too high we have struck the issue and we should handle it somehow
	next_song_start_diff=next_song_start_calculated-int(next_song["playedTime"])
	talk_delay=next_song_start_diff
	# just turn it into a positive value, makes life easier.
	if next_song_start_diff < 0: next_song_start_diff = next_song_start_diff * -1
	
	# if the next_song_start_diff is "too big", lets just add the duration of the current song
	#	to the next_song_starting_in_seconds_countdown.  that basically forces the app to wait some more.		
	if next_song_start_diff > 40: 
		start_correction = int(current_song["duration"])
		next_song_starting_in_seconds_countdown = next_song_starting_in_seconds_countdown + start_correction		
	else: start_correction = 0		

	while next_song_starting_in_seconds_countdown > 0:
		# Clear the screen.  Then display all the foo.
		os.system( [ 'clear', 'cls' ][ os.name == 'nt' ] )
		
		localtime = time.strftime("%H:%M:%S",time.localtime(time.time()))
		next_song_starting_in_seconds_countdown = (int(next_song["playedTime"]-time.time())) + settings.transmission_delay
		
		# Was; print who was playing.
		previous_song_pretty_start_time = time.strftime("%H:%M:%S",time.localtime(previous_song["playedTime"]))		
		print "%s	'%s' (%s) by %s" % (previous_song_pretty_start_time,previous_song["title"],previous_song["duration"],previous_song["artistName"])
		print "		'%s' (%s) - %s" % (previous_song["albumName"],previous_song["dateCopyrighted"],previous_song["publisher"])
		print
		
		# Now; print who is playing now
		current_song_pretty_start_time = time.strftime("%H:%M:%S",time.localtime(current_song["playedTime"]))
		
		print (Back.WHITE + Fore.BLACK + "%s	'%s' (%s) by %s" % (current_song_pretty_start_time,current_song["title"],current_song["duration"],current_song["artistName"]))
		print (Fore.RESET + Back.RESET + "		'%s' (%s) - %s" % (current_song["albumName"],current_song["dateCopyrighted"],current_song["publisher"]))
		#print "Start:	%s (duration %s)" % (current_song_pretty_start_time,current_song["duration"])
		
		# and their metatata if configured to do so.
		if settings.display_itunes_url is True: print "iTunes:	%s" % current_song_itunes_url
		if settings.display_wikipedia_url is True:	print "Wiki:	%s" % current_song_wikipedia_url
		print
		
		# Next; then who's up next and when that's set to start.  in theory ;)
		next_song_pretty_start_time = time.strftime("%H:%M:%S",time.localtime(next_song["playedTime"]))
		next_song_pretty_start_time_calculated = time.strftime("%H:%M:%S",time.localtime(next_song_start_calculated))
		
		print "%s	'%s' by %s at %s" % (next_song_pretty_start_time,next_song["title"],next_song["artistName"],next_song_pretty_start_time)
		print "		'%s' (%s) - %s" % (next_song["albumName"],next_song["dateCopyrighted"],next_song["publisher"])
		print
		
		# Time; print the current time and when we'll next update		
		if abc_miss_count == 0:	abc_hit_percentage = 100
		else: abc_hit_percentage = 100 - ((abc_miss_count / abc_hit_count) * 100)		
		
		print "- - - - - - - - - - - - - - - - - - - - - - - - - - - "
		print "%s	update in %s	api hit %s%% (hit/miss: %s / %s)" % (localtime,next_song_starting_in_seconds_countdown,abc_hit_percentage,abc_hit_count,abc_miss_count)	
		print "		transmission_delay: %s	start_correction: %s" % (settings.transmission_delay,start_correction)		
		print "		talk_delay %s (%s theirs vs %s ours)" % (talk_delay,next_song_pretty_start_time,next_song_pretty_start_time_calculated,)
		
		
		# this controls how often to update the normal now playing 'screen'.  
		time.sleep(settings.global_update_interval)
