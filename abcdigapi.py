#!/usr/bin/python -u
#
# ABC Music API module
# https://github.com/hooliowobbits/abc-dig-music
#
#  Copyright (C) 2013 Julius Roberts, hooliowobbits@gmail.com
#
# Most code borrowed from:
#  Dig Jazz Applet 
#  Copyright (C) 2007 Ian Wienand
#  http://www.wienand.org/junkcode/dig-jazz-applet/
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import urllib
import urllib2
import simplejson as json
import math
import time
import sys
from pprint import pprint

# import our own modules
import settings

def _parse_input():
	print
	print "..grabbing from %s " % settings.DIG_STATUS_XML
	#print "..grabbing song data from the ABC"

	songs = [None, None, None]
	
	#http_response = urllib2.urlopen(settings.DIG_STATUS_XML)
	#raw_info = http_response.read()
	#raw_songs = json.loads(raw_info)
	
	# Sometimes we get "simplejson.decoder.JSONDecodeError" when talking to the ABC API
	# unhandled it will crash the program.  This little loop will retry up to 5 times 
	# and then after that probably fail horriby.
	for i in range(0,5):
		while True:
			try:
				http_response = urllib2.urlopen(settings.DIG_STATUS_XML)
				raw_info = http_response.read()
				raw_songs = json.loads(raw_info)
			except ValueError:	 
				print 'ERROR!: Decoding JSON has failed, retrying in %s seconds.' % settings.sleep_after_json_decode_failed
				time.sleep(settings.sleep_after_json_decode_failed)
				continue
			break
	
	for s in raw_songs:
		
		# THIS CODE WAS WRITTEN BY Ian Wienand, AND I THINK IT'S CRASHING.  I DON'T USE TRACK NOTE ANYWAY SO I'M JUST REMMING THIS OUT.
		# clean up the tracknote so it displays
		#s['trackNote'] = s['trackNote'].replace('target="_blank"','')
		#s['trackNote'] = s['trackNote'].replace('<A HREF=','<a href=')
		#s['trackNote'] = s['trackNote'].replace('</A>','</a>')

		# put into a simple ordered list
		if s['playing'] == 'now':
			songs[0] = s
		elif s['playing'] == 'past':
			songs[1] = s
		elif s['playing'] == 'next':
			songs[2] = s
		else:
			raise TypeError
	return songs
	
#{'publisher': 'Columbia', 'dateCopyrighted': '1969', 'links': [], 'title': 'Soul Sacrifice', 'trackId': '3863-001', 'artistNote': None, 'albumName': 'Santana', 'artistName': 'Santana', 'playedTime': 1366026701, 'albumImage': 'http://www.abc.net.au/dig/covers/santana_1.jpg', 'duration': '396', 'trackNote': '<a href="http://www.santana.com" target= "_blank">Santana Website</a> ', 'playing': 'now'}
def _whats_on_now():
	songs = _parse_input()
	song = songs[0]	
	now_playing = "'" + song["title"] + "' by " + song["artistName"] + " from '" + song["albumName"] + "' (" + song["dateCopyrighted"] + ") on " + song["publisher"]
	#now_playing = "'" + song["title"] + "' by " + song["artistName"] 
	return now_playing
	
def _craft_wikipedia_url(artistName):
	# this is not implimented yet ..
	wikipedia_url = "http://wikipediafoo"
	
	return wikipedia_url
	
def _craft_itunes_url(artistName):
	print "..formulating iTunes search query string for %s" % artistName
	# "https://itunes.apple.com/search?term=tom+jones&country=au&media=music&entity=musicArtist&limit=1"
	query_args = { 'term':artistName,'country':'au','media':'music','entity':'musicArtist','limit':'1' }
	encoded_args = urllib.urlencode(query_args)
	query_itunes_url = 'https://itunes.apple.com/search?' + encoded_args
	
	# a query_itunes_url https://itunes.apple.com/search?country=au&term=Clare+Bowditch&entity=musicArtist&limit=1&media=music
	# so then we go and send that out and get something back
	print "..sending %s" % query_itunes_url
	http_response = urllib.urlretrieve(query_itunes_url)
	raw_info = open(http_response[0], "r")
	raw_foo = json.loads(raw_info.read())
	urllib.urlcleanup()
	
	# make sure we actually got something, and pass something back as appropriate
	# i think a json decode failure will crash fatally
	if raw_foo["resultCount"] == 0:
		itunes_url = "Nothing found"
	else:
		itunes_url = raw_foo["results"][0]["artistLinkUrl"]
		
	return itunes_url
